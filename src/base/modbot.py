import logging, os, yaml, re
from telegram.ext import Updater, CommandHandler, PrefixHandler, MessageHandler, Filters

class level_group():
    def __init__(self):
        self.URGENT = 0
        self.IMPORTANT = 1
        self.MIDLEVEL = 2
        self.BASIC = 3
        self.LOWEST = 4

LEVEL = level_group()

class Modbot():

    def __init__(self):
        start_data = self.__load_start_data()
        updater = Updater(
            token=start_data['SUBREDDIT_BOT_TOKEN'],
            use_context=True
        )

        self.bot = updater.bot
        self.user = updater.bot.get_me()
        self.prefixes = ["!", "$", ".", ">"]
        self.__updater = updater
        self.__job_queue = updater.job_queue
        self.__dispatcher = updater.dispatcher
        self.version = "0.1"

    def run(self):
        try:
            # Start polling tg updates
            logging.info("BOT STARTED")
            self.__updater.start_polling()
            # If it dies, do nothing TODO: restart bot
            self.__updater.idle()
    
        # THE HORROR
        except Exception as e:
            logging.critical('THE BOT HAS DIED')
            raise e

    # Add commands in a standar way, you can allways use the dispatcher to do it
    def add_command(
        self,
        name,
        handler,
        args=True,
        edit=False,
        group=LEVEL.LOWEST
    ):
        
        filters = None if edit is True else ~Filters.update.edited_message

        # Classic command
        command_handler = CommandHandler(name, handler, filters=filters, pass_args=args)
        self.__dispatcher.add_handler(command_handler, group=group)

        # Prefix commands
        prefix_handler = PrefixHandler(
            self.prefixes,
            name,
            handler,
            filters=filters,
            pass_args=args
        )

        self.__dispatcher.add_handler(prefix_handler, group=group)

    def add_job(
        self,
        callback,
        interval,
        first=None,
        last=None,
        context=None,
        name=None,
        job_kwargs=None
    ):
        self.__job_queue.run_repeating(
            callback,
            interval,
            first,
            last,
            context,
            name,
            job_kwargs
        )
        # TODO maybe add the job to a dict to shut down in a future?
        # run_custom returns an object

    def __load_start_data(self):
        return {
            "SUBREDDIT_BOT_TOKEN": os.getenv("SUBREDDIT_BOT_TOKEN")
        }


modbot = Modbot()

import logging, os, glob

# Actual dir is modules dir
modules_dir = os.path.dirname(__file__)

# Take .py files
files = os.path.join(modules_dir, "*.py")
modules = glob.glob(files)

# Load all the modules
# (__all__ is a list with all the modules, 
# doing "from src.módulos import *" loads all the modules inside __all__)
__all__ = [os.path.basename(module)[:-3]
             for module in modules
               if os.path.isfile(module) and not module.endswith('__init__.py')]


from ..base.modbot import modbot

def hello_world(update, context):
    context.bot.send_chat_action(chat_id=update.message.chat_id, action='typing')
    context.bot.send_message(chat_id=update.message.chat_id, text="Hola mundo")

modbot.add_command(name="holamundo", handler=hello_world)

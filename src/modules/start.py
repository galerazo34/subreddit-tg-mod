from ..base.modbot import modbot

def start(update, context):
    context.bot.send_chat_action(chat_id=update.message.chat_id, action='typing')
    context.bot.send_message(
        chat_id=update.message.chat_id,
        text=(
            "Este bot fue creado para poder moderar un subreddit desde telegram, "
            "mandá /help para poder ver los comandos válidos."
        )
    )

modbot.add_command(name="start", handler=start)

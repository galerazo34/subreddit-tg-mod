import logging
from telegram.ext import Updater, CommandHandler, PrefixHandler

if __name__ == '__main__':

    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO # change from INFO to DEBUG to test more and get chat_ids, user_ids, etc
    )

    # Start bot
    from src.base.modbot import modbot   
    from src.modules import *

    modbot.run()

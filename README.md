# subreddit-tg-mod

A bot to mod a subreddit from a telegram channel

## Installation

- Clone this repo

- Install/upgrade python 3

- Install/upgrade pip3:

    `$ python3 -m pip install --upgrade pip`

- Install python libraries: 

    `$ pip3 install -r requirements.txt`

- Create an environment variable in your system called SUBREDDIT_BOT_TOKEN

- Place the bot token as value for that env var

## Usage
- From a terminal in the main folder run python3 bot.py
- Use ctrl + C twice to kill the bot

## Support
There is no support xddd, except for this: if you want to modify folders please take care with
how the python packages works when you use directories (PLZ DO NOT DELETE src DIRECTORY)

## Roadmap
- Moderate a subreddit from a telegram channel

## Contributing
You can make merge requests!

## Authors and acknowledgment
Some virgin guys that use both reddit and telegram so you can imagine how we are

## License
See LICENSE

## Project status
In progres
